import React from "react"
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import Discover from "./screens/Discover"


import './App.css';
import 'antd/dist/antd.css'; 

function App() {
  return (
    <Router>
      <div>
        <Switch>
          <Route path="/">
            <Discover />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;


function About() {
  return <h2>About</h2>;
}

function Users() {
  return <h2>Users</h2>;
}
