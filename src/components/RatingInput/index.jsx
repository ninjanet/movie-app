import Rater from 'react-rater'
import 'react-rater/lib/react-rater.css'

const RatingInput = ({ratingValue, setRatingValue, ...props}) => {
  return <Rater {...props} total={5} rating={ratingValue || 0} onRate={({rating}) => {
      if(ratingValue === rating) {
        return setRatingValue(0)
      }
      return setRatingValue(rating)
    }}/>
}

export default RatingInput;