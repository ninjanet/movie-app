import { Card } from 'antd';
import styled from 'styled-components';

const StyledCard = styled(Card)`
  width: 300px;
`

const MovieCard = ({ movie, setCurrentMovie }) => {
  const { poster_path, title } = movie;
  const imgBase = "https://image.tmdb.org/t/p/w500";
  
  return (
    <StyledCard title={title} extra={<a onClick={() => setCurrentMovie(movie)} >More</a>} cover={<img  src={`${imgBase}${poster_path}`} alt={title} loading="lazy" />} />
  ); 
}

export default MovieCard;