import React from "react";
import { Input } from 'antd';

const SearchInput = (props) => {
  return (<Input placeholder="Movie search" style={{ width: 400 }} {...props} />); 
}

export default SearchInput;