import { Card } from 'antd';
import RatingInput from '../RatingInput'
import styled from 'styled-components';

const StyledCard = styled(Card)`
  flex-direction: row;
  display: flex;
  align-items: center;

`;

const StyledImg = styled.img`
  width: 200px;
`
const { Meta } = Card;

const MovieDetail = ({ movie }) => {
  const { poster_path, title } = movie;
  const imgBase = "https://image.tmdb.org/t/p/w500";
  
  return (
    <StyledCard
      cover={<StyledImg  src={`${imgBase}${poster_path}`} alt={title} loading="lazy" />}
    >
    <RatingInput ratingValue={ movie && movie.vote_average / 2} interactive={false} ></RatingInput>
    <Meta
      title={movie.original_title} 
      description={movie.overview}
    />
  </StyledCard>
); 
}

export default MovieDetail;