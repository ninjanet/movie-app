
import React, { useState } from "react";

import MovieCard from '../MovieCard'
import MovieDetail from '../MovieDetail'
import useFetch from "../../hooks/useFetch"
import { Modal } from 'antd';

import styled from 'styled-components';

const MoviesContainer = styled.div`
    flex-wrap: wrap;
    display: flex;
    justify-content: space-between;
`

const MovieList = ({search, ratingValue}) => {
    const BASE = "https://api.themoviedb.org/3";
    const url = `${BASE}/discover/movie?api_key=d139fe92b0bb2b6ad21bffbf849372b8&&sort_by=popularity.desc`;
    const searchUrl = `${BASE}/search/movie?api_key=d139fe92b0bb2b6ad21bffbf849372b8&query=${search}`;
    const filterUrl = ratingValue && `${BASE}/discover/movie?api_key=d139fe92b0bb2b6ad21bffbf849372b8&vote_average.lte=${ratingValue*2}&vote_average.gte=${ratingValue*2-2}`;

    const requestUrl = (() => {
        if(search !== '') {
            return searchUrl
        } else if(ratingValue) {
            return filterUrl
        }
        return url
    })()
    const {response, isLoading} = useFetch(requestUrl, {});
    const apiRequestResults = response?.results
    const [currentMovie, setCurrentMovie] = useState();

    const handleOk = () => {
        setCurrentMovie(undefined);
    };

    return (
      <div>
          <MoviesContainer>
            {
              isLoading ? (
                "Loading..."
              ) : (
                apiRequestResults && apiRequestResults.map(movie => (
                  <MovieCard key={movie.id} movie={movie} setCurrentMovie={setCurrentMovie}/>
                ))
              )
            }
        </MoviesContainer>
        <Modal title={currentMovie?.title} visible={!!currentMovie} onOk={handleOk} closable={false} cancelButtonProps={{ style: { display: 'none' } }}>
            <MovieDetail movie={currentMovie} />
        </Modal>
      </div>
    );
  }

  export default MovieList;
  
  