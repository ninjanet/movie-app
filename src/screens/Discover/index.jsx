import React, {useState} from "react";

import {SearchInput, MovieList, RatingInput} from "../../components"

import styled from 'styled-components'

const Screen = styled.div`
  flex-direction:column;
  flex-wrap: wrap;
`

const FilterSearchContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
  width: 500px;
  height: 100px;
  margin: auto;
`

const Discover = () => {
  const [search, setSearch ] = useState('')
  const [ratingValue, setRatingValue ] = useState()

  return (
    <Screen>
      <div>
        <FilterSearchContainer>
          <RatingInput ratingValue={ratingValue} setRatingValue={(rating) => {
            setRatingValue(rating)
            setSearch('')
          }}></RatingInput>
          <SearchInput onChange={(e) =>  {
            setSearch(e.target.value)
            setRatingValue(undefined)
          }} value={search} />
        </FilterSearchContainer>
        <MovieList search={search} ratingValue={ratingValue} />
      </div>
    </Screen>
    
  );
}

export default Discover;